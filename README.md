# Sneaker Impact

## Documentation
1. [Nuxt 3](https://v3.nuxtjs.org/)
2. [Vite](https://vitejs.dev/guide/)
3. [Tailwind](https://tailwindcss.com/)
4. [Supabase](https://supabase.com/)

## Development
`npm i` & `npm run dev`

## Deployment
Gitlab CI/CD to Netlify
