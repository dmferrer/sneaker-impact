import { Provider } from '@supabase/supabase-js';

export async function useInitAuth() {
  const client = useSupabaseClient();
  const user = useSupabaseUser();

  const { data: profile } = await useAsyncData(
    'user_profile',
    async () => {
      if (!user.value) return null;
      return await client
        .from('profiles')
        .select('*')
        .eq('id', user.value.id)
        .single();
    },
    { watch: [() => user.value?.id] }
  );

  const userWithProfile = computed(() => {
    if (!user.value || !profile.value) return null;
    return { ...user.value, profile: { ...profile.value.data } };
  });

  async function login(provider: Provider) {
    return await client.auth.signIn(
      { provider }
    );
  }

  async function logout() {
    await client.auth.signOut();
  }

  return { user: userWithProfile, login, logout };
}

export function provideAuth(authData: Awaited<ReturnType<typeof useInitAuth>>) {
  provide('auth', authData);
}

export function useAuth() {
  return inject<Awaited<ReturnType<typeof useInitAuth>>>('auth');
}